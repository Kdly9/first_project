#include "generator.h"
#include "sort.h"

#include <chrono>
#include <time.h>

void print (const std::vector<int>& toPrint) {
  if (!toPrint.empty()) {
    for (const int & i : toPrint) {
      std::cout << i << " ";
    }
    std::cout << std::endl;
  }
}
void sortIndex(std::vector<int>& vec) {
  for (std::size_t i = 0; i < vec.size() - 1; i++) {
    for (std::size_t j = i + 1; j < vec.size(); j++) {
      if (i % 2 == 0 && j % 2 == 0) {
        if (vec[i] < vec[j]) {
          std::swap(vec[i], vec[j]);
        }
      }
      if (i % 2 != 0 && j % 2 != 0) {
        if (vec[i] > vec[j]) {
          std::swap(vec[i], vec[j]);
        }
      }
    }
  }
}

void fSort(std::vector<int>& vec) {
  std::cout << "generated vector:" << std::endl;
  print(vec);
  vec.erase(std::remove_if(vec.begin(),
    vec.end(),
    [](int x) { return (x % 2 == 0); }),
    vec.end());
  std::cout << "first part:" << std::endl;
  std::sort(vec.begin(), vec.end());
  print(vec);
}

void sSort(std::vector<int>& vec) {
  std::cout << "generated vector:" << std::endl;
  print(vec);
  vec.erase(std::remove_if(vec.begin(),
    vec.end(),
    [](int x) { return x % 2; }),
    vec.end());
  std::reverse(vec.begin(), vec.end());
  std::cout << "second part:" << std::endl;
  print(vec);
}

void tSort(std::vector<int>& vec) {
  std::cout << "generated vector:" << std::endl;
  print(vec);
  sortIndex(vec);
  std::cout << "third part:" << std::endl;
  print(vec);
}

int main (){
  unsigned int input;
  std::cout << "1 - generating numbers " << "2 - manual input" << std::endl;
  std::cin >> input;
  std::vector<int> vec;
  DataGen generate;
  SortArray matrix;
  std::unique_ptr<int> mat;
  if (input == 1)
  {
    std::size_t size;
    int first, last;
    std::cout << "size of array:" << std::endl;
    std::cin >> size;
    std::cout << "start of range:" << std::endl;
    std::cin >> first;
    std::cout << "end of range:" << std::endl;
    std::cin >> last;
    vec = generate.getVectorData(size, first, last);
    fSort(vec);

    vec = generate.getVectorData(size, first, last);
    sSort(vec);

    vec = generate.getVectorData(size, first, last);
    tSort(vec);

    mat = generate.getArray(size, first, last);
    matrix.firstPart(std::move(mat), size);

    mat = generate.getArray(size, first, last);
    matrix.secondPart(mat, size);

    mat = generate.getArray(size, first, last);
    matrix.thirdPart(mat, size);

  }
  else if (input == 2) {
    std::size_t size;
    int inp;
    std::cout << "size of array 1 :" << std::endl;
    std::cin >> size;
    std::unique_ptr<int> sp0 = std::make_unique<int>(size);
    std::cout << "data:" << std::endl;
    for (std::size_t i = 0; i < size; ++i)
    {
      while (!(std::cin >> inp)) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Invalid input. Try again: ";
      }
      vec.emplace_back(inp);
      sp0.get()[i] = inp;
    }

    fSort(vec);
    matrix.firstPart(std::move(sp0), size);

    std::cout << "size of array 2 :" << std::endl;
    std::cin >> size;
    std::unique_ptr<int> sp1 = std::make_unique<int>(size);
    std::cout << "data:" << std::endl;
    for (std::size_t i = 0; i < size; ++i)
    {
      while (!(std::cin >> inp)) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Invalid input. Try again: ";
      }
      vec.emplace_back(inp);
      sp1.get()[i] = inp;
    }

    sSort(vec);
    matrix.secondPart(sp1, size);

    std::cout << "size of array 3 :" << std::endl;
    std::cin >> size;
    std::unique_ptr<int> sp2 = std::make_unique<int>(size);
    std::cout << "data:" << std::endl;
    for (std::size_t i = 0; i < size; ++i)
    {
      while (!(std::cin >> inp)) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Invalid input. Try again: ";
      }
      vec.emplace_back(inp);
      sp2.get()[i] = inp;
    }

    tSort(vec);
    matrix.thirdPart(sp2, size);
  }

  return 0;
}
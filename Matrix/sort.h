#pragma once
#include <memory>


class SortArray {
public:
  SortArray() = default;

  void firstPart(std::unique_ptr<int> matrix, std::size_t size);
  void secondPart(std::unique_ptr<int>& matrix, std::size_t size);
  void thirdPart(std::unique_ptr<int>& matrix, std::size_t size);
private:

  void printArray(std::unique_ptr<int>& matrix, std::size_t size);
  void insertionSort(std::unique_ptr<int>& matrix, std::size_t size);
  void reverse(std::unique_ptr<int>& matrix, std::size_t size);

};

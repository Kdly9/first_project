#include "sort.h"

#include <algorithm>
#include <cstring>
#include <iostream>

void SortArray::firstPart(std::unique_ptr<int> matrix, std::size_t size) {
  std::cout << "generated matrix: " << std::endl;
  printArray(matrix, size);
  std::size_t n = 0;
  for (int i = 0; i < size; i++)
    if (matrix.get()[i] % 2)
      matrix.get()[n++] = matrix.get()[i];
  std::unique_ptr<int> arr(new int[n]);
  for (std::size_t i = 0; i < n; i++) {
    arr.get()[i] = matrix.get()[i];
  }
  insertionSort(arr, n);
  std::cout << "first part:" << std::endl;
  printArray(arr, n);
}

void SortArray::secondPart(std::unique_ptr<int>& matrix, std::size_t size) {
  std::size_t n = 0;
  std::cout << "generated matrix: " << std::endl;
  printArray(matrix, size);
  for (int i = 0; i < size; i++)
    if (matrix.get()[i] % 2 == 0)
      matrix.get()[n++] = matrix.get()[i];
  std::unique_ptr<int> arr(new int[n]);
  for (int i = 0; i < n; i++) {
    arr.get()[i] = matrix.get()[i];
  }
  reverse(arr, n);
  std::cout << "second part:" << std::endl;
  printArray(arr, n);
  arr.reset();
}

void SortArray::thirdPart(std::unique_ptr<int>& matrix, std::size_t size) {
  std::cout << "generated matrix: " << std::endl;
  printArray(matrix, size);
  for (std::size_t i = 0; i < size - 1; i++) {
    for (std::size_t j = i + 1; j < size; j++) {
      if (i % 2 == 0 && j % 2 == 0) {
        if (matrix.get()[i] < matrix.get()[j]) {
          std::swap(matrix.get()[i], matrix.get()[j]);
        }
      }
      if (i % 2 != 0 && j % 2 != 0) {
        if (matrix.get()[i] > matrix.get()[j]) {
          std::swap(matrix.get()[i], matrix.get()[j]);
        }
      }
    }
  }
  std::cout << "third part:" << std::endl;
  printArray(matrix, size);
}

void SortArray::printArray(std::unique_ptr<int>& matrix, std::size_t size) {
  int i;
  for (i = 0; i < size; i++)
    std::cout << matrix.get()[i] << " ";
  std::cout << std::endl;
}

void SortArray::insertionSort(std::unique_ptr<int>& matrix, std::size_t size){
  std::size_t j;
  int current;
  for (std::size_t i = 1; i < size; i++) {
    current = matrix.get()[i];
    j = i - 1;
    while (j >= 0 && matrix.get()[j] > current) {
      matrix.get()[j + 1] = matrix.get()[j];
      j = j - 1;
    }
    matrix.get()[j + 1] = current;
  }
}

void SortArray::reverse(std::unique_ptr<int>& matrix, std::size_t size) {
  for (std::size_t i = 0; i < size / 2; ++i) {
    std::swap(matrix.get()[i], matrix.get()[size - i - 1]);
  }
}
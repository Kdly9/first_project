#pragma once
#include <iostream>
#include <random>
#include <vector>

class DataGen {
public:

  DataGen() = default;

  std::vector<int> getVectorData(std::size_t size, int& first, int& last);
  std::unique_ptr<int> getArray(std::size_t size, int& first, int& last);

private:

  int getRnd(int& lower_bound, int& upper_bound);

  template< typename T >
  struct array_deleter
  {
    void operator ()(T const* p)
    {
      delete[] p;
    }
  };
};
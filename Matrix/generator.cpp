#include "generator.h"

#include <random>
#include <stdlib.h>




std::vector<int> DataGen::getVectorData(std::size_t size, int& first, int& last) {
  std::vector<int> rezult;
  for (std::size_t i = 0; i < size; ++i) {
    rezult.emplace_back(getRnd(first, last));
  }
  return rezult;
}

std::unique_ptr<int> DataGen::getArray(std::size_t size, int& first, int& last) {
  std::unique_ptr<int> ar(new int[size]);
  for (std::size_t i = 0; i < size; ++i) {
    ar.get()[i] = getRnd(first, last);
  }
  return ar;
}

int DataGen::getRnd(int& lower_bound, int& upper_bound) {
  std::uniform_int_distribution<int> distr(lower_bound, upper_bound);
  std::random_device rand_dev;
  std::mt19937 rand_engine(rand_dev());
  int rez = distr(rand_engine);
  return rez;
}